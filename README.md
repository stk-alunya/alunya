# Alunya the Cat

This is a repo for the **Alunya the Cat** add-on kart for SuperTuxKart.

Since this kart is intended for use in the SuperTuxKart add-on repo, please avoid using assets that aren't shared under [an acceptable license](https://supertuxkart.net/Licensing) and update the license where necessary. Licensing details are available in License.txt if you wish to re-use assets in your own games or mods.

Improvements and variants are welcome. Documentation for making karts can be found here: [SuperTuxKart - Making Karts](https://supertuxkart.net/Making_Karts)

Search online for forks and pull requests before pulling in case this repo becomes abandoned as this is a single-purpose account that might never be monitored.

Blender 2.78 has been used so it may be easier to continue using it. Beware that Blender can store file-paths in a .blend which could include your login name or a unique folder structure, so consider overwriting that data with `sed` or a hex editor before pushing if you are paranoid.